﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Media3D;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Accord.Collections;
using ConnectedComponentLabeling;
using OxyPlot;
using OxyPlot.Series;
using Microsoft.Win32;
using MoreLinq;

namespace Metrics
{
    public static class SpatialMetrics
    {
        // These metrics quantify aspects of the spaital dose distribution that cannot be calculated from DVH parameters.

        #region Spatial dose gradient

        /// <summary>
        /// Finds the maximum dose at a certain distance from a structure contour. Distance must be at least 3mm.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="distanceInMM"></param>
        /// <returns></returns>
        public static double FindDoseAtDistance(PlanSetup plan, Structure structure, double distanceInMM)
        {
            if (plan.Dose == null || !structure.HasSegment)
                return double.NaN;

            string name = "temp";
            if (plan.StructureSet.CanAddStructure("CONTROL", name))
            {
                SegmentVolume segmentOuter = structure.Margin(marginInMM: distanceInMM + plan.Dose.ZRes);
                Structure OuterMargin = plan.StructureSet.AddStructure("CONTROL", "a_" + name);
                OuterMargin.ConvertToHighResolution();
                OuterMargin.SegmentVolume = segmentOuter;

                SegmentVolume segmentInner = structure.Margin(marginInMM: distanceInMM);
                Structure InnerMargin = plan.StructureSet.AddStructure("CONTROL", "i_" + name);
                InnerMargin.ConvertToHighResolution();
                InnerMargin.SegmentVolume = segmentInner;


                SegmentVolume segmentShell = OuterMargin.Sub(InnerMargin);
                Structure newShell = plan.StructureSet.AddStructure("CONTROL", "s_" + name);
                newShell.ConvertToHighResolution();
                newShell.SegmentVolume = segmentShell;

                DVHData DVH = plan.GetDVHCumulativeData(newShell, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

                double gradient = DVH.MaxDose.Dose;

                // Remove temporary structures
                plan.StructureSet.RemoveStructure(OuterMargin);
                plan.StructureSet.RemoveStructure(InnerMargin);
                plan.StructureSet.RemoveStructure(newShell);

                return gradient;
            }

            return double.NaN;
        }

        /// <summary>
        /// Finds the maximum dose at a certain distance from a structure contour. Distance must be at least 3mm. Overload for PlanUncertainties.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="distanceInMM"></param>
        /// <returns></returns>
        public static double FindDoseAtDistance(PlanSetup plan, PlanUncertainty upln, Structure structure, double distanceInMM)
        {
            if (upln.Dose == null || !structure.HasSegment)
                return double.NaN;

            string name = "temp";
            if (plan.StructureSet.CanAddStructure("CONTROL", name))
            {
                SegmentVolume segmentOuter = structure.Margin(marginInMM: distanceInMM + plan.Dose.ZRes);
                Structure OuterMargin = plan.StructureSet.AddStructure("CONTROL", "a_" + name);
                OuterMargin.ConvertToHighResolution();
                OuterMargin.SegmentVolume = segmentOuter;

                SegmentVolume segmentInner = structure.Margin(marginInMM: distanceInMM);
                Structure InnerMargin = plan.StructureSet.AddStructure("CONTROL", "i_" + name);
                InnerMargin.ConvertToHighResolution();
                InnerMargin.SegmentVolume = segmentInner;


                SegmentVolume segmentShell = OuterMargin.Sub(InnerMargin);
                Structure newShell = plan.StructureSet.AddStructure("CONTROL", "s_" + name);
                newShell.ConvertToHighResolution();
                newShell.SegmentVolume = segmentShell;

                DVHData uDVH = upln.GetDVHCumulativeData(newShell, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

                double gradient = uDVH.MaxDose.Dose;

                // Remove temporary structures
                plan.StructureSet.RemoveStructure(OuterMargin);
                plan.StructureSet.RemoveStructure(InnerMargin);
                plan.StructureSet.RemoveStructure(newShell);

                return gradient;
            }

            return double.NaN;
        }

        /// <summary>
        /// Method that returns the dose gradient towards an OAR, defined as the distance to an isodose. Returns a list of tuples, each being (shortest distance, mean over 10% points with shortest distances).
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="OAR"></param>
        /// <param name="percentage"></param>
        /// <param name="doses"></param>
        /// <returns></returns>
        public static Tuple<List<ScatterPoint>, List<ScatterPoint>> GetOARGradientCurvePoints(PlanSetup plan, Structure OAR, double[] doses)
        {
            List<ScatterPoint> maxCurve = new List<ScatterPoint>();
            List<ScatterPoint> percCurve = new List<ScatterPoint>();
            List<Tuple<double, Structure>> doseStructures = new List<Tuple<double,Structure>>();

            // percentage to use for mean of closest points
            double percentage = 10;

            if (plan.StructureSet.CanAddStructure("CONTROL", "Dose_" + doses[0].ToString() + "GY"))
            {

                // Collect all isodose structures in a list
                // Check if removing works correctly!!!
                foreach (var dose in doses)
                {
                    if (dose > 0)
                    {
                        var isoStruct = plan.StructureSet.AddStructure("CONTROL", "Dose_" + dose.ToString() + "GY");
                        isoStruct.ConvertDoseLevelToStructure(plan.Dose, new DoseValue(dose, DoseValue.DoseUnit.Gy));
                        doseStructures.Add(new Tuple<double, Structure>(dose, isoStruct));
                    }
                }

                // Calculate distances and collect in list
                foreach (var tup in doseStructures)
                {
                    Tuple<double, double> dist = FindOARGradient(OAR, tup.Item2, percentage);
                    double dose = tup.Item1;

                    // Check if isodose croses structure
                    DVHData dvh = plan.GetDVHCumulativeData(OAR, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    if (dose <= dvh.MaxDose.Dose)
                    {
                        maxCurve.Add(new ScatterPoint(dose, 0));
                        percCurve.Add(new ScatterPoint(dose, 0));
                    }
                    else
                    {
                        maxCurve.Add(new ScatterPoint(dose, dist.Item1));
                        percCurve.Add(new ScatterPoint(dose, dist.Item2));
                    }
                }

                // remember to remove all isodose structures!
                while (plan.StructureSet.Structures.Any(s => s.Id.Contains("Dose_")))
                {
                    Structure st = plan.StructureSet.Structures.First(s => s.Id.Contains("Dose_"));
                    plan.StructureSet.RemoveStructure(st);
                }

            }

            return new Tuple<List<ScatterPoint>, List<ScatterPoint>>(maxCurve, percCurve);
        }

        /// <summary>
        /// Method that finds the shortest distances between an OAR and an isodose structure
        /// </summary>
        /// <param name="OAR"></param>
        /// <param name="isodose"></param>
        /// <returns></returns>
        public static Tuple<double, double> FindOARGradient(Structure OAR, Structure isodose, double percentage)
        {
            // initialize lists of points
            List<double[]> treePointsList = new List<double[]>();
            List<double[]> queryPointsList = new List<double[]>();

            // Initialize list to hold all min distances
            List<double> distances = new List<double>();

            // Isodose points go in the tree. OAR points are query points.
            foreach (var point in isodose.MeshGeometry.Positions)
            {
                treePointsList.Add(new double[] { point.X, point.Y, point.Z });
            }

            foreach (var point in OAR.MeshGeometry.Positions)
            {
                queryPointsList.Add(new double[] { point.X, point.Y, point.Z });
            }

            // populate tree
            KDTree<double> tree = KDTree.FromData<double>(treePointsList.ToArray());

            // Find shortest distance for each query point
            foreach (var qPoint in queryPointsList)
            {
                var neighbors = tree.Nearest(qPoint, neighbors: 1);

                distances.Add(neighbors.First().Distance);
            }

            // return shortest distance and average over shortest x%
            distances.Sort();
            return new Tuple<double, double>(distances.Min(), distances.Take((int)Math.Floor(distances.Count() * percentage / 100.0)).Average());
        }

        /// <summary>
        /// Method to calculate the distance from a structure's contour to a given isodose curve on a given axial plane z. Calculates the dose along a line using ESAPI's GetDoseToPoint(VVector point). The direction input must be either "A", "P", "L", "R", "lowL", "lowR", "hornL", or "hornR"
        /// </summary>
        /// <param name="plan">The current PlanSetup</param>
        /// <param name="structure">The chosen Structure</param>
        /// <param name="isodoseLevel"></param>
        /// <param name="direction">String. Must be "A", "P", "L", or "R"</param>
        /// <param name="z">Int. Image plane number</param>
        /// <param name="centerX">Double. The coordinate for the AP line along which the distance to the isodose is found.</param>
        /// <param name="centerY">Double. The coordinate for the upper LR line along which the distance to the isodose is found.</param>
        /// <param name="searchRes">Grid along which points are sampled.</param>
        /// <param name="doseRes">How close a point's dose must be to the evaluation dose to be considered on the isodose line.</param>
        /// <returns></returns>
        public static double GetDistanceToIsodoseAlongAxis(PlanSetup plan, Structure structure, double isodoseLevel, char direction, int z, double cX, double cY, double searchRes, double doseRes)
        {
            // initialize variables
            double dist = double.NaN;
            Dose dose = plan.Dose;
            double count = 0; // this is in order to take the average over up to five lines later

            // shift centerX and centerY to be in relation to beam isocenter
            VVector iso = plan.Beams.FirstOrDefault(b => !b.IsSetupField).IsocenterPosition;
            double centerX = iso.x + cX;
            double centerY = iso.y + cY;

            // Get contours
            VVector[][] structureContours = structure.GetContoursOnImagePlane(z);

            // If structure has no contours on the plane return NaN
            if (structureContours.Length < 1)
            {
                // debug
                //System.Windows.MessageBox.Show("No contours found on image plane.");
                return dist;
            }

            double zCoor = plan.Dose.Origin.z + z * plan.Dose.ZRes; // coordinate of the plane index

            // collect all contour points in a list. For the case that there might be several separate contours on a plane.
            var contourPoints = new List<VVector>();

            foreach (var contourArray in structureContours)
            {
                foreach (VVector point in contourArray)
                {
                    contourPoints.Add(point);
                }
            }

            // define axis along which to get the dose values

            // initiate variables
            var line = new List<VVector>();
            double minX = dose.Origin.x;
            double minY = dose.Origin.y;
            double maxX = dose.Origin.x + (dose.XSize * dose.XRes);
            double maxY = dose.Origin.y + (dose.YSize * dose.YRes);
            VVector pointOnLine = new VVector();

            // find min and max x and y values for segment definition
            double maxYLoc = contourPoints.First().y;
            double minYLoc = contourPoints.First().y;
            double maxXLoc = contourPoints.First().x;
            double minXLoc = contourPoints.First().x;

            foreach (VVector point in contourPoints) // for each point in the current contour segment
            {
                if (point.y < minYLoc)
                {
                    minYLoc = point.y;
                }
                else if (point.y > maxYLoc)
                {
                    maxYLoc = point.y;
                }

                if (point.x < minXLoc)
                {
                    minXLoc = point.x;
                }
                else if (point.x > maxXLoc)
                {
                    maxXLoc = point.x;
                }
            }


            // Anterior
            if (direction == 'A')
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {
                    // contour points on the line

                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxYLoc) + 10);
                    int profileStart = (int)(Math.Floor(minYLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(centerX + k, profileStart, zCoor), new VVector(centerX + k, profileEnd, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break; // stop search when a point is found. Saves a lot of time.
                        }
                    }

                    // if the structure does not lie on the search line
                    if (!found)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (double y = pointOnLine.y; y > minY; y -= searchRes)
                    {
                        VVector point = new VVector(pointOnLine.x, y, pointOnLine.z);
                        double pointDose = dose.GetDoseToPoint(point).Dose;
                        if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                        {
                            dist += GetDistance3D(point, pointOnLine);
                            count += 1;
                            break;
                        }
                    }
                }
            }
            // Posterior
            else if (direction == 'P')
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {

                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxYLoc) + 10);
                    int profileStart = (int)(Math.Floor(minYLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(centerX + k, profileStart, zCoor), new VVector(centerX + k, profileEnd, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = segment.Count() - 1; i > 0; i--)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break;
                        }
                    }

                    // if the structure does not lie on the search line
                    if (!found)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (double y = pointOnLine.y; y < maxY; y += searchRes)
                    {
                        VVector point = new VVector(pointOnLine.x, y, pointOnLine.z);
                        double pointDose = dose.GetDoseToPoint(point).Dose;
                        if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                        {
                            dist += GetDistance3D(point, pointOnLine);
                            count += 1;
                            break;
                        }
                    }
                }
            }

            // Left
            else if (direction == 'L')
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {
                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxXLoc) + 10);
                    int profileStart = (int)(Math.Floor(minXLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(profileStart, centerY + k, zCoor), new VVector(profileEnd, centerY + k, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = segment.Count() - 1; i > 0; i--)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break;
                        }
                    }
                    // if the structure does not lie on the search line
                    if (!found | pointOnLine.x < centerX)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (double x = pointOnLine.x; x < maxX; x += searchRes)
                    {
                        VVector point = new VVector(x, pointOnLine.y, pointOnLine.z);
                        double pointDose = dose.GetDoseToPoint(point).Dose;
                        if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                        {
                            dist += GetDistance3D(point, pointOnLine);
                            count += 1;
                            break;
                        }
                    }
                }
            }

            // right
            else if (direction == 'R')
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {
                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxXLoc) + 10);
                    int profileStart = (int)(Math.Floor(minXLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(profileStart, centerY + k, zCoor), new VVector(profileEnd, centerY + k, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break;
                        }
                    }

                    // if structure does not cross line return zero
                    if (!found | pointOnLine.x > centerX)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (double x = pointOnLine.x; x > minX; x -= searchRes)
                    {
                        VVector point = new VVector(x, pointOnLine.y, pointOnLine.z);
                        double pointDose = dose.GetDoseToPoint(point).Dose;
                        if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                        {
                            dist += GetDistance3D(point, pointOnLine);
                            count += 1;
                            break;
                        }
                    }
                }
            }
            else
            {
                // debug
                //System.Windows.MessageBox.Show("Direction must be either A, P, L, or R!");
                return dist;
            }

            if (count > 0) // to avoid dividing by zero and returning NaN if PTV is not on the line
            {
                return dist / count;
            }
            else
            {
                // debug
                //System.Windows.MessageBox.Show("Count of calculations was zero.");
                return dist;
            }
        }

        /// <summary>
        /// Method to calculate the distance from a structure's contour to a given isodose curve on a given axial plane z. Calculates the dose along a line using ESAPI's GetDoseToPoint(VVector point). The direction input must be either "A", "P", "L", "R", "lowL", "lowR", "hornL", or "hornR"
        /// </summary>
        /// <param name="plan">The current PlanSetup</param>
        /// <param name="structure">The chosen Structure</param>
        /// <param name="isodoseLevel"></param>
        /// <param name="direction">String. Must be "A", "P", "L", or "R"</param>
        /// <param name="z">Int. Image plane number</param>
        /// <param name="centerX">Double. The coordinate for the AP line along which the distance to the isodose is found.</param>
        /// <param name="centerY">Double. The coordinate for the upper LR line along which the distance to the isodose is found.</param>
        /// <param name="searchRes">Grid along which points are sampled.</param>
        /// <param name="doseRes">How close a point's dose must be to the evaluation dose to be considered on the isodose line.</param>
        /// <returns></returns>
        public static double GetDistanceToIsodoseTowardPoint(PlanSetup plan, Structure structure, VVector dirPoint, double isodoseLevel, int z, double centerX, double centerY, double searchRes, double doseRes)
        {
            double dist = double.NaN;
            Dose dose = plan.Dose;
            double count = 0; // this is in order to take the average over up to five lines later

            // Get contours
            VVector[][] structureContours = structure.GetContoursOnImagePlane(z);

            // If structure has no contours on the plane return NaN
            if (structureContours.Length < 1)
            {
                return dist;
            }

            double zCoor = plan.Dose.Origin.z + z * plan.Dose.ZRes; // coordinate of the plane index

            // collect all contour points in a list. For the case that there might be several separate contours on a plane.
            var contourPoints = new List<VVector>();

            foreach (var contourArray in structureContours)
            {
                foreach (VVector point in contourArray)
                {
                    contourPoints.Add(point);
                }
            }

            // define axis along which to get the dose values

            // initiate variables
            var line = new List<VVector>();

            VVector pointOnLine = new VVector();

            // find distance
            dist = 0;

            // take average over five adjacent lines to increase robustness
            for (double k = -2; k <= 2; k += 1)
            {
                // contour points on the line

                // define vector between center points on the z plane
                VVector vec = (new VVector(dirPoint.x, dirPoint.y, zCoor) - new VVector(structure.CenterPoint.x, structure.CenterPoint.y, zCoor));
                VVector unitVec = vec / vec.Length;

                // find perpendicular vector for multiple calculations along parallel lines. set x = 1, y = -1 + vec.x + vec.y                
                VVector perpVec = new VVector(1, -1 + vec.x + vec.y, 0);
                VVector perpUnitVec = perpVec / perpVec.Length;

                // initiate buffer for segment profile
                System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((int)Math.Ceiling(vec.Length) * 10); // resolution : ca. 0.1mm


                // get segment profile
                var segment = structure.GetSegmentProfile(new VVector(dirPoint.x + k * perpUnitVec.x, dirPoint.y + k * perpUnitVec.y, zCoor), new VVector(structure.CenterPoint.x + k * perpUnitVec.x, structure.CenterPoint.y + k * perpUnitVec.y, zCoor), segmentBuffer);
                bool found = false;

                for (int i = 0; i < segment.Count(); i++)
                {
                    if (segment[i].Value) // first point met when moving towards structure
                    {
                        pointOnLine = segment[i].Position;
                        found = true;
                        break; // stop search when a point is found. Saves a lot of time.
                    }
                }

                // if the structure does not lie on the search line
                if (!found)
                {
                    dist += 0;
                    continue;
                }

                // search points and stop when desired dose is reached. 
                for (double j = 0; j < vec.Length; j += searchRes)
                {
                    VVector point = new VVector(pointOnLine.x + j * unitVec.x, pointOnLine.y + j * unitVec.y, pointOnLine.z);
                    double pointDose = dose.GetDoseToPoint(point).Dose;
                    if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                    {
                        dist += GetDistance3D(point, pointOnLine);
                        count += 1;
                        break;
                    }
                }
            }


            if (count > 0) // to avoid dividing by zero and returning NaN if PTV is not on the line
            {
                return dist / count;
            }
            else
            {
                return dist;
            }
        }


        #endregion

        #region Cold and hot spots

        /// <summary>
        /// Method to find cold spots and return their size and distance from a reference contour. Distance is given as the shortest distance of the spot's COM to the contour.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="image"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static List<Tuple<double, double>> FindColdSpotLocations(PlanSetup plan, Structure structure, double cutoff)
        {
            var CSLoc = new List<Tuple<double, double>>();

            var dose = plan.Dose;

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, true);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.ProcessReturnPixels(binaryDoseMatrix, cutoff);

            // populate KDtree with structure contour points
            List<double[]> treeList = new List<double[]>();

            foreach (var point in structure.MeshGeometry.Positions)
            {
                treeList.Add(new double[] { point.X, point.Y, point.Z });
            }

            KDTree<double> tree = KDTree.FromData<double>(treeList.ToArray());

            // Loop over cold spots and find COM shortest distance to tree
            foreach (var cs in ColdSpots)
            {
                double size = cs.Value.Count(p => p.dose > 0); // in voxels
                double sumX = cs.Value.Sum(p => p.Position.X);
                double sumY = cs.Value.Sum(p => p.Position.Y);
                double sumZ = cs.Value.Sum(p => p.Position.Z);

                // find COM in matrix coordinates
                VVector com = new VVector(sumX / size, sumY / size, sumZ / size);

                // convert matrix coordinates to image coordinates
                double[] comCoordinates = new double[] { dose.Origin.x + (com.x * dose.XRes), dose.Origin.y + (com.y * dose.YRes), dose.Origin.z + (com.z * dose.ZRes) };

                // find distance to structure contour
                double dist = tree.Nearest(comCoordinates, neighbors: 1).First().Distance;

                // Add to return dictionary
                double sizeCC = size / 1000; // dose matrix res = 1 x 1 x 1 mm
                CSLoc.Add(new Tuple<double, double>(sizeCC, dist));
            }

            return CSLoc;
        }

        /// <summary>
        /// Method to find hot spots in a structure and their distance from a target structure's contour. Distance is given as the shortest distance of the spot's COM to the contour.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="image"></param>
        /// <param name="structure"></param>
        /// <param name="target"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static List<Tuple<double, double>> FindHotSpotLocations(PlanSetup plan, Structure structure, Structure target, double cutoff)
        {
            var CSLoc = new List<Tuple<double, double>>();

            var dose = plan.Dose;

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, false);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var HotSpots = ccl.ProcessReturnPixels(binaryDoseMatrix, cutoff);

            // populate KDtree with structure contour points
            List<double[]> treeList = new List<double[]>();

            foreach (var point in target.MeshGeometry.Positions)
            {
                treeList.Add(new double[] { point.X, point.Y, point.Z });
            }

            KDTree<double> tree = KDTree.FromData<double>(treeList.ToArray());

            foreach (var cs in HotSpots)
            {
                double size = cs.Value.Count(p => p.dose > 0); // in voxels
                double sumX = cs.Value.Sum(p => p.Position.X);
                double sumY = cs.Value.Sum(p => p.Position.Y);
                double sumZ = cs.Value.Sum(p => p.Position.Z);

                // find COM in matrix coordinates
                VVector com = new VVector(sumX / size, sumY / size, sumZ / size);

                // convert matrix coordinates to image coordinates
                double[] comCoordinates = new double[] { dose.Origin.x + (com.x * dose.XRes), dose.Origin.y + (com.y * dose.YRes), dose.Origin.z + (com.z * dose.ZRes) };

                // find distance to structure contour
                double dist = tree.Nearest(comCoordinates, neighbors: 1).First().Distance;

                // Add to return dictionary
                double sizeCC = size * dose.XRes * dose.YRes * dose.ZRes / 1000;

                if (target.IsPointInsideSegment(new VVector(comCoordinates[0], comCoordinates[1], comCoordinates[2])))
                {
                    CSLoc.Add(new Tuple<double, double>(sizeCC, -dist));
                }
                else
                {
                    CSLoc.Add(new Tuple<double, double>(sizeCC, dist));
                }
            }

            return CSLoc;
        }

        /// <summary>
        /// Method that finds the volume of a cold spot and the nearest and Hausdorff distances to a reference structure (target)
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="target"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static List<Tuple<double, double[]>> FindCSNearFarDistance(PlanSetup plan, Structure target, double cutoff)
        {
            // initialize list for return values
            List<Tuple<double, double[]>> retVals = new List<Tuple<double, double[]>>();

            // initialize list for point coordinates
            List<double[]> treePoints = new List<double[]>();

            // Add target contour points to list
            foreach (var point in target.MeshGeometry.Positions)
            {
                treePoints.Add(new double[] { point.X, point.Y, point.Z });
            }

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            // Get binary 3D dose matrix for the target
            var binMat = DoseMatrixExtract.CreateBinaryDoseMatrix(plan.Dose, target, cutoff, xres, yres, zres, true);

            // Find cold spots
            BinaryCCL coldCCL = new BinaryCCL();
            var coldSpots = coldCCL.ProcessReturnPixels(binMat, cutoff);

            // populate KDtree
            KDTree<double> tree = KDTree.FromData<double>(treePoints.ToArray());

            // Loop over cold spots
            foreach (var cs in coldSpots)
            {
                // initialize list to save distance to each cold spot voxel
                List<double> distances = new List<double>();
                double vol = 0;

                foreach (var px in cs.Value)
                {
                    distances.Add(tree.Nearest(new double[] { plan.Dose.Origin.x + (px.Position.X), plan.Dose.Origin.y + (px.Position.Y), plan.Dose.Origin.z + (px.Position.Z) }, neighbors: 1).First().Distance);
                    //distances.Add(tree.Nearest(new double[] { plan.Dose.Origin.x + (px.Position.X * plan.Dose.XRes), plan.Dose.Origin.y + (px.Position.Y * plan.Dose.YRes), plan.Dose.Origin.z + (px.Position.Z * plan.Dose.ZRes) }, neighbors: 1).First().Distance);
                    vol += 1.0 / 1000; // each voxel adds this volume in cc to the total cold spot. Res 1 x 1 x 1 mm. This will slightly over-estimate the volume compared to the interpolated dose visible in the Eclipse UI, especially for small spots, depending on the dose matrix resolution chosen
                }

                if (vol >= 0.03)
                {
                    distances.Sort();
                    retVals.Add(new Tuple<double, double[]>(vol, new double[] { distances.Min(), distances.Max(), distances[(int)Math.Ceiling(distances.Count() * 0.05)], distances[(int)Math.Floor(distances.Count() * 0.95)] }));
                }
            }

            return retVals;
        }

        /// <summary>
        /// Method that finds the volume of hot spots in an OAR as well as the nearest and Hausdorff distances to a reference contour (target)
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="target"></param>
        /// <param name="OAR"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static List<Tuple<double, double[]>> FindHSNearFarDistance(PlanSetup plan, Structure target, Structure OAR, double cutoff)
        {
            // initialize list for return values
            List<Tuple<double, double[]>> retVals = new List<Tuple<double, double[]>>();

            // initialize list for point coordinates
            List<double[]> treePoints = new List<double[]>();

            // Add target contour points to list
            foreach (var point in target.MeshGeometry.Positions)
            {
                treePoints.Add(new double[] { point.X, point.Y, point.Z });
            }

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            // Get binary 3D dose matrix for the OAR
            var binMat = DoseMatrixExtract.CreateBinaryDoseMatrix(plan.Dose, OAR, cutoff, xres, yres, zres, false);

            // Find cold spots
            BinaryCCL hotCCL = new BinaryCCL();
            var hotSpots = hotCCL.ProcessReturnPixels(binMat, cutoff);

            // populate KDtree
            KDTree<double> tree = KDTree.FromData<double>(treePoints.ToArray());

            // Loop over cold spots
            foreach (var hs in hotSpots)
            {
                // initialize list to save distance to each cold spot voxel
                List<double> distances = new List<double>();
                double vol = 0;

                foreach (var px in hs.Value)
                {
                    distances.Add(tree.Nearest(new double[] { plan.Dose.Origin.x + (px.Position.X), plan.Dose.Origin.y + (px.Position.Y), plan.Dose.Origin.z + (px.Position.Z) }, neighbors: 1).First().Distance);
                    //distances.Add(tree.Nearest(new double[] { plan.Dose.Origin.x + (px.Position.X * plan.Dose.XRes), plan.Dose.Origin.y + (px.Position.Y * plan.Dose.YRes), plan.Dose.Origin.z + (px.Position.Z * plan.Dose.ZRes) }, neighbors: 1).First().Distance);
                    vol += 1.0 / 1000; // each pixel adds this volume in cc to the total cold spot. This will slightly over-estimate the volume compared to the interpolated dose visible in the Eclipse UI, especially for small spots, depending on the dose matrix resolution chosen
                }

                if (vol >= 0.03)
                {
                    distances.Sort();
                    retVals.Add(new Tuple<double, double[]>(vol, new double[] { distances.Min(), distances.Max(), distances[(int)Math.Ceiling(distances.Count() * 0.05)], distances[(int)Math.Floor(distances.Count() * 0.95)] }));
                }
            }

            return retVals;
        }

        /// <summary>
        /// Method that gets the overlap-volume histogram of cold volumes and a reference structure (also called the dose-location histogram)
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="image"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static List<DataPoint> GetColdOVH(PlanSetup plan, Structure structure, Image image, double cutoff)
        {
            List<VVector> structurePoints = FindPointsInsideStructure(plan, image, structure);
            List<double> distances = new List<double>();
            List<DataPoint> points = new List<DataPoint>();
            List<double[]> treePoints = new List<double[]>();

            // populate KDtree
            foreach (var point in structure.MeshGeometry.Positions)
            {
                treePoints.Add(new double[] { point.X, point.Y, point.Z });
            }

            KDTree<double> tree = KDTree.FromData<double>(treePoints.ToArray());

            // Loop over points in the structure
            foreach (var point in structurePoints)
            {
                double pointDose = plan.Dose.GetDoseToPoint(point).Dose;

                if (pointDose < cutoff)
                {
                    var dist = tree.Nearest(new double[] { point.x, point.y, point.z }, neighbors: 1).First().Distance;
                    distances.Add(dist);
                }
            }

            // Avoids error when calling distances.Max() in the if-clause
            if (distances.Count() == 0)
            {
                return points;
            }

            for (double i = 0; i < distances.Max() + 1; i += 0.1)
            {
                // each distance corresponds to one voxel. This will slightly overestimate the total volume because voxels might partly extend beyond the structure, and because DVH dose sampling may be different.
                double vol = distances.Count(d => d <= i) * image.XRes * image.YRes * image.ZRes / 1000;
                var p = new DataPoint(i, vol);
                points.Add(p);
            }

            return points;
        }

        /// <summary>
        /// Method that calculates the overlap-volume histogram of hot volumes with a reference structure (also called a dose-location histogram)
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure1"></param>
        /// <param name="structure2"></param>
        /// <param name="image"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static List<DataPoint> GetHotOVH(PlanSetup plan, Structure structure1, Structure structure2, Image image, double cutoff)
        {
            var structurePoints = FindPointsInsideStructure(plan, image, structure1);
            List<double> distances = new List<double>();
            List<DataPoint> points = new List<DataPoint>();
            List<double[]> treePoints = new List<double[]>();

            // populate KDtree
            foreach (var point in structure2.MeshGeometry.Positions)
            {
                treePoints.Add(new double[] { point.X, point.Y, point.Z });
            }

            KDTree<double> tree = KDTree.FromData<double>(treePoints.ToArray());

            // Loop over points in the structure
            foreach (var point in structurePoints)
            {
                double pointDose = plan.Dose.GetDoseToPoint(point).Dose;

                if (pointDose > cutoff)
                {
                    var dist = tree.Nearest(new double[] { point.x, point.y, point.z }, neighbors: 1).First().Distance;

                    if (structure2.IsPointInsideSegment(point)) // assign negative distance to point inside target
                    {
                        distances.Add(-dist);
                    }
                    else
                    {
                        distances.Add(dist);
                    }
                }
            }

            if (distances.Count() == 0)
            {
                return points;
            }

            for (double i = distances.Min(); i < distances.Max() + 1; i += 0.1) // min value: target's effective radius
            {
                double vol = distances.Count(d => d <= i) * image.XRes * image.YRes * image.ZRes / 1000;
                var p = new DataPoint(i, vol);
                points.Add(p);
            }
            return points;
        }
        #endregion

        #region Methods for heat maps
        /// <summary>
        /// Returns a 2D array for a heat map plot of the dose at a certain distance from a structure.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static double[,] CalculateDoseAtDistance(PlanSetup plan, Structure structure, double distance)
        {
            double[,] map = new double[360, 180];

            VVector com = structure.CenterPoint;

            for (int phi = 0; phi < 360; phi++)
            {
                double phiR = phi * 2 * Math.PI / 360.0;

                for (int theta = 0; theta < 180; theta++)
                {
                    double thetaR = theta * 2 * Math.PI / 360.0;

                    // Define line in direction specified by theta,phi
                    // x = R * sin(theta) * cos(phi)
                    // y = R * sin(theta) * sin(phi)
                    // z = R * cos(theta)
                    VVector endPoint = com + 200 * new VVector(Math.Sin(thetaR) * Math.Cos(phiR), Math.Sin(thetaR) * Math.Sin(phiR), Math.Cos(thetaR));

                    VVector unitV = (endPoint - com) / 200;

                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray(200); // 1mm resolution
                    SegmentProfile segment = structure.GetSegmentProfile(com, endPoint, segmentBuffer);
                    VVector contourPoint = new VVector();

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (!segment[i].Value)
                        {
                            contourPoint = segment[i - 1].Position;
                            break;
                        }
                    }

                    map[phi, theta] = plan.Dose.GetDoseToPoint(contourPoint + distance * unitV).Dose;
                }
            }

            // debug
            //string path = GetCsvSavePath();
            //using (var sw = new StreamWriter(path))
            //{
            //    for (int i = 0; i < map.GetLength(0); i++)
            //    {
            //        for (int j = 0; j < map.GetLength(1); j++)
            //        {
            //            sw.Write(string.Format("{0};", map[i, j]));
            //        }
            //        sw.WriteLine();
            //    }
            //}

            return map;
        }

        /// <summary>
        /// Returns a 2D array for a heat map plot of the distance from a structure contour to a certain dose level. This takes a long time to calculate. Make more efficiant before implementing in PlanComparison script.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="dose"></param>
        /// <returns></returns>
        public static double[,] CalculateDistanceToDose(PlanSetup plan, Structure structure, double dose)
        {
            double[,] map = new double[360, 180];

            VVector com = structure.CenterPoint;

            for (int phi = 0; phi < 360; phi++)
            {
                double phiR = phi * 2 * Math.PI / 360.0;

                for (int theta = 0; theta < 180; theta++)
                {
                    double thetaR = theta * 2 * Math.PI / 360.0;

                    // Define line in direction specified by theta,phi
                    // x = R * sin(theta) * cos(phi)
                    // y = R * sin(theta) * sin(phi)
                    // z = R * cos(theta)
                    VVector endPoint = com + 200 * new VVector(Math.Sin(thetaR) * Math.Cos(phiR), Math.Sin(thetaR) * Math.Sin(phiR), Math.Cos(thetaR));

                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray(200); // 0.1mm resolution
                    double[] doseBuffer = new double[200]; // 0.1mm res
                    SegmentProfile segment = structure.GetSegmentProfile(com, endPoint, segmentBuffer);
                    DoseProfile doseP = plan.Dose.GetDoseProfile(com, endPoint, doseBuffer);
                    int contourPointIndex = 0;

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (!segment[i].Value)
                        {
                            contourPointIndex = i - 1;
                            break;
                        }
                    }

                    for (int j = contourPointIndex; j < doseP.Count; j++)
                    {
                        if (Math.Abs(doseP[j].Value - dose) < 0.2) // consider changing resolution
                        {
                            map[phi, theta] = (j - contourPointIndex) / 10;
                            break;
                        }
                        else
                        {
                            // map[phi, theta] = double.NaN;
                            map[phi, theta] = 5;
                        }
                    }
                }
            }

            // debug
            string path = GetCsvSavePath();
            using (var sw = new StreamWriter(path))
            {
                for (int i = 0; i < map.GetLength(0); i++)
                {
                    for (int j = 0; j < map.GetLength(1); j++)
                    {
                        sw.Write(string.Format("{0};", map[i, j]));
                    }
                    sw.WriteLine();
                }
            }

            return map;
        }

        #endregion

        #region Beam path quantification

        /// <summary>
        /// Method that calculates the volume of healthy brain that is in the entrance path of IMPT beams. Can be used to quantify the quality of beam directions.
        /// This could be generalized to any OAR.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        public static double[] GetBeamPathROIVolsBrain(PlanSetup plan, Structure structure)
        {
            double[] returnArray = new double[plan.Beams.Count(b => !b.IsSetupField)];


            // loop over beams
            int beamCount = 0;
            foreach (var beam in plan.Beams)
            {
                if (plan.PlanType == PlanType.ExternalBeam_Proton)
                {
                    IonBeam ionBeam = (IonBeam)beam;
                    Structure beamTarget = ionBeam.TargetStructure;

                    // Get position of source
                    double beamAngle = ionBeam.IonControlPoints.First().GantryAngle;
                    VVector beamSourcePosition = ionBeam.GetSourceLocation(beamAngle);

                    //// debug
                    //string printPoints = "";
                    //printPoints += string.Format("{0}\t{1}\t{2}\n", beamSourcePosition.x, beamSourcePosition.y, beamSourcePosition.z);
                    //printPoints += string.Format("{0}\t{1}\t{2}\n", ionBeam.IsocenterPosition.x, ionBeam.IsocenterPosition.y, ionBeam.IsocenterPosition.z);

                    // define direction parallel to source-isocenter vector. Direction: isocenter to source
                    VVector sourceToIsoCenterVector = beamSourcePosition - ionBeam.IsocenterPosition;
                    VVector unitVector = sourceToIsoCenterVector / sourceToIsoCenterVector.Length; // unit length should be 1mm

                    // initialize point in plane
                    VVector planePoint = new VVector();

                    // Define plane of search start points perpendicular to the source-isocenter line
                    // Eq for a plane: a(x-x0) + b(y-y0) + c(z-z0) = 0. (a,b,c): normal vector. (x0,y0,z0): point in plane

                    // The plane should lie behind the target structure. Move back along the source to isocenter line until no longer inside Brain structure

                    for (int i = 1; i < 1000; i++) // I guess after a meter you should be outside the body contour
                    {
                        VVector point = ionBeam.IsocenterPosition - i * unitVector;
                        if (!structure.IsPointInsideSegment(point) & !beamTarget.IsPointInsideSegment(point))
                        {
                            planePoint = point;
                            break;
                        }
                    }

                    // find random vector in the plane
                    VVector randomPlanePoint = new VVector(planePoint.x, planePoint.y + 5 / unitVector.y, planePoint.z - 5 / unitVector.z);
                    VVector planeVector1 = randomPlanePoint - planePoint;
                    VVector planeUnitVector1 = planeVector1 / planeVector1.Length;

                    // find second plane vector. Cross product of unit normal vector and unit first plane vector gives orthogonal unit vector
                    VVector planeVector2 = new VVector(unitVector.y * planeUnitVector1.z - unitVector.z * planeUnitVector1.y, unitVector.z * planeUnitVector1.x - unitVector.x * planeUnitVector1.z, unitVector.x * planeUnitVector1.y - unitVector.y * planeUnitVector1.x);
                    VVector planeUnitVector2 = planeVector2 / planeVector2.Length;



                    // line search with start points in the plane. Be sure to define the plane large enough and the search lines far enough to capture all of the healthy tissue between target and source
                    for (int i = -200; i < 200; i += 1) // 40 cm should cover the average head, right?
                    {
                        for (int j = -200; j < 200; j += 1)
                        {
                            // Get target segment profile
                            VVector startPoint = planePoint + i * planeUnitVector1 + j * planeUnitVector2;
                            VVector endPoint = startPoint + 1000 * unitVector;
                            System.Collections.BitArray segmentStride = new System.Collections.BitArray(1000);

                            SegmentProfile targetProfile = beamTarget.GetSegmentProfile(startPoint, endPoint, segmentStride);

                            // if target is on the line
                            if (targetProfile.Any(p => p.Value))
                            {
                                // find point where target ends and VOI starts
                                VVector targetEndPoint = targetProfile.Last(p => p.Value).Position;
                                System.Collections.BitArray voiStride = new System.Collections.BitArray(1000);

                                SegmentProfile voiProfile = structure.GetSegmentProfile(targetEndPoint, endPoint, voiStride);
                                if (voiProfile.Any(p => p.Value))
                                {
                                    VVector structureStartPoint = voiProfile.First(p => p.Value).Position;
                                    VVector structureEndPoint = voiProfile.Last(p => p.Value).Position;
                                    returnArray[beamCount] += GetDistance3D(structureStartPoint, structureEndPoint) / 1000;
                                    // since both the plane grid resolution and the segmentProfile resolution are 1mm the answer should come out in mm^3
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }

                    beamCount++;
                }
            }


            return returnArray;
        }

        #endregion

        #region Others
        /// <summary>
        /// Method to get dose profile on line between two structures
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static double[,] FindDoseOnLine(PlanSetup plan, Structure s1, Structure s2)
        {
            VVector cm1 = s1.CenterPoint;
            VVector cm2 = s2.CenterPoint;

            double dist = Math.Sqrt(Math.Pow((cm1.x - cm2.x), 2) + Math.Pow((cm1.y - cm2.y), 2) + Math.Pow((cm1.z - cm2.z), 2)); // distance in mm
            double[] buffer = new double[(int)Math.Ceiling(dist)];
            System.Collections.BitArray bufferS1 = new System.Collections.BitArray((int)Math.Ceiling(dist));
            System.Collections.BitArray bufferS2 = new System.Collections.BitArray((int)Math.Ceiling(dist));
            DoseProfile profile = plan.Dose.GetDoseProfile(cm1, cm2, buffer);
            SegmentProfile segment1 = s1.GetSegmentProfile(cm1, cm2, bufferS1);
            SegmentProfile segment2 = s2.GetSegmentProfile(cm1, cm2, bufferS2);

            double[,] doseArray = new double[profile.Count(), 3];

            for (int i = 0; i < profile.Count(); i++)
            {
                doseArray[i, 0] = i * (dist / buffer.Length);
                doseArray[i, 1] = profile[i].Value;

                if (bufferS1[i] & bufferS2[i])
                {
                    doseArray[i, 2] = 3;
                }
                else if (bufferS1[i])
                {
                    doseArray[i, 2] = 1;
                }
                else if (bufferS2[i])
                {
                    doseArray[i, 2] = 2;
                }
                else
                {
                    doseArray[i, 2] = 0;
                }
            }

            return doseArray;
        }
        /// <summary>
        /// Returns spatial DVH for a given structure. The spatial DVH is taken as "shells" of the structure. Returns DVH sequence from outer to inner.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="margins">Margins for the shells. Give the distance from structure edge to each shell. Only three volumes can be calculated: two margin inputs needed..</param>
        /// <returns></returns>
        public static DVHPoint[][] GetSpatialDvh(PlanSetup plan, Structure structure, List<double> margins)
        {
            // sort in ascending order
            margins.Sort();

            // initialize array of dvhpoint arrays
            DVHPoint[][] returnArray = new DVHPoint[margins.Count()][];
            if (plan.StructureSet.CanAddStructure("CONTROL", structure.Id + "_inner"))
            {
                SegmentVolume cropVolMid = structure.Margin(-margins.ElementAt(0));
                SegmentVolume cropVolInner = structure.Margin(-margins.ElementAt(1));
                SegmentVolume structFarSegment = cropVolInner;
                SegmentVolume structMidSegment = cropVolMid.Sub(structFarSegment);
                SegmentVolume structNearSegment = structure.Sub(structMidSegment).Sub(structFarSegment);

                Structure OARnear = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_outer");
                OARnear.SegmentVolume = structNearSegment;
                Structure OARmid = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_mid");
                OARmid.SegmentVolume = structMidSegment;
                Structure OARfar = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_inner");
                OARfar.SegmentVolume = structFarSegment;

                if (!OARnear.IsEmpty)
                {
                    DVHData nearDVH = plan.GetDVHCumulativeData(OARnear, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[0] = nearDVH.CurveData;
                }

                if (!OARmid.IsEmpty)
                {
                    DVHData midDVH = plan.GetDVHCumulativeData(OARmid, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[1] = midDVH.CurveData;
                }

                if (!OARfar.IsEmpty)
                {
                    DVHData farDVH = plan.GetDVHCumulativeData(OARfar, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[2] = farDVH.CurveData;
                }

                plan.StructureSet.RemoveStructure(OARnear);
                plan.StructureSet.RemoveStructure(OARmid);
                plan.StructureSet.RemoveStructure(OARfar);
            }


            return returnArray;
        }

        /// <summary>
        /// Returns spatial DVH for a given structure. The spatial DVH is derived by cropping the main structure with the crop structure.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="cropStructure"></param>
        /// <param name="margins">Margins to crop with.</param>
        /// <returns></returns>
        public static DVHPoint[][] GetSpatialDvh(PlanSetup plan, Structure structure, Structure cropStructure, List<double> margins)
        {
            // sort in ascending order
            margins.Sort();

            // initialize array of dvhpoint arrays
            DVHPoint[][] returnArray = new DVHPoint[margins.Count() + 1][];

            if (plan.StructureSet.CanAddStructure("CONTROL", structure.Id + "_near"))
            {
                SegmentVolume cropVolMid = cropStructure.Margin(margins.ElementAt(0));
                SegmentVolume cropVolFar = cropStructure.Margin(margins.ElementAt(1));
                SegmentVolume structFarSegment = structure.Sub(cropVolFar);
                SegmentVolume structMidSegment = structure.Sub(cropVolMid).Sub(structFarSegment);
                SegmentVolume structNearSegment = structure.Sub(structMidSegment).Sub(structFarSegment);

                Structure OARnear = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_near");
                OARnear.SegmentVolume = structNearSegment;
                Structure OARmid = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_mid");
                OARmid.SegmentVolume = structMidSegment;
                Structure OARfar = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_far");
                OARfar.SegmentVolume = structFarSegment;

                if (!OARnear.IsEmpty)
                {
                    DVHData nearDVH = plan.GetDVHCumulativeData(OARnear, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[0] = nearDVH.CurveData;
                }

                if (!OARmid.IsEmpty)
                {
                    DVHData midDVH = plan.GetDVHCumulativeData(OARmid, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[1] = midDVH.CurveData;
                }

                if (!OARfar.IsEmpty)
                {
                    DVHData farDVH = plan.GetDVHCumulativeData(OARfar, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[2] = farDVH.CurveData;
                }

                plan.StructureSet.RemoveStructure(OARnear);
                plan.StructureSet.RemoveStructure(OARmid);
                plan.StructureSet.RemoveStructure(OARfar);
            }
            return returnArray;
        }


        #endregion

        #region Helper methods

        /// <summary>
        /// Interpolates linearly between two points on the same xy-plane and returns desired value at parameter t (x or y)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        private static VVector InterpolateLinear(VVector a, VVector b, double t, string retval)
        {
            double incl = (b.y - a.y) / (b.x - a.x);
            double intercept = a.y - (incl * a.x);

            if (retval.ToUpper() == "Y")
            {
                return new VVector(t, (incl * t) + intercept, a.z);
            }
            else if (retval.ToUpper() == "X")
            {
                return new VVector((t - intercept) / incl, t, a.z);
            }
            else return new VVector(double.NaN, double.NaN, double.NaN);
        }

        /// <summary>
        /// Method to convert DVH points to OxyPlot DataPoint objects. Returns array of DataPoint objects.
        /// </summary>
        /// <param name="dvhCurve">DVHPoint[] dvhCurve</param>
        /// <returns></returns>
        private static DataPoint[] ConvertToDataPoints(DVHPoint[] dvhCurve)
        {
            DataPoint[] dataArray = new DataPoint[dvhCurve.Length];

            for (int i = 0; i < dvhCurve.Length; i++)
            {
                dataArray[i] = new DataPoint(dvhCurve[i].DoseValue.Dose, dvhCurve[i].Volume);
            }

            return dataArray;
        }

        /// <summary>
        /// Returns 2D distance between a pair of points (x1, y1) and (x2, y2)
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        /// <param name="y1"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        private static double GetDistance2D(double x1, double x2, double y1, double y2)
        {
            double dist = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
            return dist;
        }

        /// <summary>
        /// Calculate the 3D distance between two points.
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        private static double GetDistance3D(Point3D point1, Point3D point2)
        {
            double dist = Math.Sqrt(Math.Pow((double)point1.X - (double)point2.X, 2) + Math.Pow((double)point1.Y - (double)point2.Y, 2) + Math.Pow((double)point1.Z - (double)point2.Z, 2));
            return dist;
        }

        /// <summary>
        /// Calculate the 3D distance between two points.
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        private static double GetDistance3D(VVector point1, VVector point2)
        {
            double dist = Math.Sqrt(Math.Pow((double)point1.x - (double)point2.x, 2) + Math.Pow((double)point1.y - (double)point2.y, 2) + Math.Pow((double)point1.z - (double)point2.z, 2));
            return dist;
        }

        /// <summary>
        /// Method to get a list of all grid points (VVector objects) inside a given structure
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="image"></param>
        /// <param name="structure"></param>
        /// <returns>List of VVectors points</returns>
        public static List<VVector> FindPointsInsideStructure(PlanSetup plan, Image image, Structure structure)
        {
            Dose dose = plan.Dose;
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(image.XSize);

            List<VVector> points = new List<VVector>();



            for (int z = 0; z < image.ZSize; z += 1)
            {
                for (int y = 0; y < image.YSize; y += 1)
                {
                    VVector start = image.Origin +
                                    image.YDirection * y * image.YRes +
                                    image.ZDirection * z * image.ZRes;
                    VVector end = start + image.XDirection * image.XRes * image.XSize;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);

                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            points.Add(segmentProfile[i].Position);
                        }
                    }
                }
            }
            return points;
        }

        private static bool IsStructureOnImagePlane(Structure structure, int z)
        {
            // initialize return variable
            bool retVar = false;

            // get contours
            VVector[][] contours = structure.GetContoursOnImagePlane(z + 1); // There is a bug in the ESAPI method here from what I can tell. GetContoursOnImagePlane(z) returns contours for z+1.

            // are there contours on this plane?
            if (contours.Length > 0)
            {
                retVar = true;
            }

            return retVar;
        }

        

        public static double GetSurfaceArea(Structure isodose)
        {
            var points = isodose.MeshGeometry.Positions;
            var triangleIndices = isodose.MeshGeometry.TriangleIndices;
            double area = 0;

            for (int i = 0; i < triangleIndices.Count() / 3; i++)
            {
                List<Point3D> triPoints = new List<Point3D>();
                Point3D a = points.ElementAt(triangleIndices.ElementAt(i * 3));
                Point3D b = points.ElementAt(triangleIndices.ElementAt((i * 3) + 1));
                Point3D c = points.ElementAt(triangleIndices.ElementAt((i * 3) + 2));

                Vector3D ab = new Vector3D(c.X - a.X, c.Y - a.Y, c.Z - a.Z);
                Vector3D ac = new Vector3D(b.X - a.X, b.Y - a.Y, b.Z - a.Z);

                double abDOTac = (ab.X * ac.X) + (ab.Y * ac.Y) + (ab.Z * ac.Z);

                if (ab.Length > 0 && ac.Length > 0)
                {
                    var val = 0.5 * ab.Length * ac.Length * Math.Sqrt(1 - Math.Pow(abDOTac / (ab.Length * ac.Length), 2));
                    if (!double.IsNaN(val))
                        area += val;
                }
            }
            return area / 100;
        }

        private static double GetIsodoseVolume(Isodose isodose)
        {
            Point3D v1;
            Point3D v2;
            Point3D v3;
            var points = isodose.MeshGeometry.Positions;
            var triangleIndices = isodose.MeshGeometry.TriangleIndices;
            double vol = 0;

            for (int i = 0; i < triangleIndices.Count() / 3; i++)
            {
                List<Point3D> triPoints = new List<Point3D>();
                v1 = points.ElementAt(triangleIndices.ElementAt(i * 3));
                v2 = points.ElementAt(triangleIndices.ElementAt((i * 3) + 1));
                v3 = points.ElementAt(triangleIndices.ElementAt((i * 3) + 2));
                vol += (((v2.Y - v1.Y) * (v3.Z - v1.Z) - (v2.Z - v1.Z) * (v3.Y - v1.Y)) * (v1.X + v2.X + v3.X)) / 6;
            }
            return vol / 1000; // cmm to cc
        }

        // For debugging
        private static string GetCsvSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export plot data to csv file",
                Filter = "txt files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
        #endregion

        #region Old or unused methods

        /// <summary>
        /// Spatial DVH for OARs divided into regions by distance to target.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="oar"></param>
        /// <param name="target"></param>
        /// <param name="border1"></param>
        /// <param name="border2"></param>
        /// <returns></returns>
        public static DataPoint[][] GetSpatialDvhOar(PlanSetup plan, Structure oar, Structure target, double border1, double border2)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            // Three DVHs
            DataPoint[][] returnArray = new DataPoint[3][];

            // sort margin distances from lowest to highest
            List<double> margins = new List<double> { border1, border2 };
            margins.Sort();

            if (plan.StructureSet.CanAddStructure("CONTROL", oar.Id[0] + oar.Id[1] + oar.Id[2] + oar.Id[3] + oar.Id[4] + "_near"))
            {
                SegmentVolume cropVolMid = target.Margin(margins.ElementAt(0));
                SegmentVolume cropVolFar = target.Margin(margins.ElementAt(1));

                SegmentVolume structSegmentFar = oar.Sub(cropVolFar);
                SegmentVolume cropSegmentMid = oar.Sub(cropVolMid);

                Structure OAR0 = plan.StructureSet.AddStructure("CONTROL", oar.Id[0] + oar.Id[1] + oar.Id[2] + oar.Id[3] + oar.Id[4] + "_far");
                try
                {
                    OAR0.SegmentVolume = structSegmentFar;
                }
                catch { }


                Structure OARcropMid = plan.StructureSet.AddStructure("CONTROL", oar.Id[0] + oar.Id[1] + oar.Id[2] + oar.Id[3] + oar.Id[4] + "_tm");
                try
                {
                    OARcropMid.SegmentVolume = cropSegmentMid;
                }
                catch { }

                SegmentVolume structSegmentMid = oar.Sub(cropVolMid).Sub(structSegmentFar);
                SegmentVolume structSegmentNear = oar.Sub(OARcropMid);

                Structure OAR1 = plan.StructureSet.AddStructure("CONTROL", oar.Id[0] + oar.Id[1] + oar.Id[2] + oar.Id[3] + oar.Id[4] + "_mid");
                try
                {
                    OAR1.SegmentVolume = structSegmentMid;
                }
                catch { }

                Structure OAR2 = plan.StructureSet.AddStructure("CONTROL", oar.Id[0] + oar.Id[1] + oar.Id[2] + oar.Id[3] + oar.Id[4] + "_near");

                try
                {
                    OAR2.SegmentVolume = structSegmentNear;
                }
                catch { }


                if (!OAR0.IsEmpty) // far
                {
                    DVHData DVH0 = plan.GetDVHCumulativeData(OAR0, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[0] = ConvertToDataPoints(DVH0.CurveData);
                }
                else
                {
                    returnArray[0] = new DataPoint[] { new DataPoint(0, 0) };
                }

                if (!OAR1.IsEmpty) // mid
                {
                    DVHData DVH1 = plan.GetDVHCumulativeData(OAR1, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[1] = ConvertToDataPoints(DVH1.CurveData);
                }
                else
                {
                    returnArray[1] = new DataPoint[] { new DataPoint(0, 0) };
                }

                if (!OAR2.IsEmpty) // near
                {
                    DVHData DVH2 = plan.GetDVHCumulativeData(OAR2, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[2] = ConvertToDataPoints(DVH2.CurveData);
                }
                else
                {
                    returnArray[2] = new DataPoint[] { new DataPoint(0, 0) };
                }

                plan.StructureSet.RemoveStructure(OAR0);
                plan.StructureSet.RemoveStructure(OAR1);
                plan.StructureSet.RemoveStructure(OAR2);
                plan.StructureSet.RemoveStructure(OARcropMid);
            }

            return returnArray;
        }

        /// <summary>
        /// Brute force method to find minimum distance between structure and isodose of a desired dose level (in Gy). Override for uncertainty plans.
        /// </summary>
        /// <param name="plan">PlanSetup object</param>
        /// <param name="structure">Structure object</param>
        /// <param name="isodoseLevel">double</param>
        /// <returns></returns>
        public static double GetDistanceToIsodose(PlanSetup plan, PlanUncertainty upln, Structure structure, double isodoseLevel)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;
            Dose udose = upln.Dose;
            Dose dose = plan.Dose;
            var dist = double.NaN;

            // If dose or structure are undefined
            if (udose == null || !structure.HasSegment)
                return double.NaN;

            // If the structure and isodose overlap
            DVHData uDVH = upln.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

            if (!(uDVH.MaxDose.Dose >= isodoseLevel))
            {
                // Get the desired isodose's 3D Meshgrid points
                if (plan.StructureSet.CanAddStructure("CONTROL", "Iso_" + isodoseLevel))
                {
                    Structure Iso = plan.StructureSet.AddStructure("CONTROL", "Iso_" + isodoseLevel);
                    Iso.ConvertDoseLevelToStructure(udose, new DoseValue(isodoseLevel, DoseValue.DoseUnit.Gy));
                    var IsoMeshPoints = Iso.MeshGeometry.Positions;

                    // Get the structure's 3D Meshgrid points
                    var StructMeshPoints = structure.MeshGeometry.Positions;

                    // Find minimum distance between the two point collections. 
                    double minDist = double.PositiveInfinity;
                    foreach (var isoPoint in IsoMeshPoints)
                    {
                        foreach (var structPoint in StructMeshPoints)
                        {
                            double tempDist = GetDistance3D(isoPoint, structPoint);
                            if (tempDist < minDist)
                                minDist = tempDist;
                        }
                    }

                    dist = minDist;
                    plan.StructureSet.RemoveStructure(Iso);
                }
            }


            return dist;
        }

        /// <summary>
        /// Method to find distance from a given point to a structure's center of mass. Points inside a given structure can be found with the method FindPointsInsideStructure().
        /// </summary>
        /// <param name="point"></param>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="effRadUnits">1: Distance given in units of the effective structure radius. 0: Distance given in mm.</param>
        /// <returns></returns>
        public static double FindStructureCMDist(VVector point, PlanSetup plan, Structure structure, bool effRadUnits)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            VVector CM = structure.CenterPoint;

            double effRad = Math.Pow((3.0 / 4.0) * Math.PI, (1.0 / 3.0)) * Math.Pow(structure.Volume, (1.0 / 3.0));

            double CMdistance = Math.Sqrt(Math.Pow(point.x - CM.x, 2) + Math.Pow(point.y - CM.y, 2) + Math.Pow(point.z - CM.z, 2));

            if (effRadUnits)
            {
                return CMdistance / effRad;
            }
            else
            {
                return CMdistance;
            }
        }

        /// <summary>
        /// Method to find shortest distance from a given point to a structure's contour. Points inside a given structure can be found with the method FindPointsInsideStructure().
        /// </summary>
        /// <param name="point"></param>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        public static double FindStructureEdgeDist(VVector point, Image image, Structure structure)
        {
            Dictionary<VVector, double> allPlaneDist = new Dictionary<VVector, double>();

            for (int z = 0; z < image.ZSize; z++) // consider limiting z range to speed up code
            {
                var contours = structure.GetContoursOnImagePlane(z);

                if (contours.Count() > 0)
                {
                    double planeMinDist = 1000; // arbitrarily large initial value
                    double planePointDist;
                    VVector minVec = new VVector();

                    // find closest distance to contour point and save
                    for (int i = 0; i < contours.Count(); i++)
                    {
                        for (int j = 0; j < contours[i].Count(); j++)
                        {
                            planePointDist = Math.Sqrt(Math.Pow((point.x - contours[i][j].x), 2) + Math.Pow(point.y - contours[i][j].y, 2) + Math.Pow(point.z - contours[i][j].z, 2));

                            if (planePointDist < planeMinDist)
                            {
                                planeMinDist = planePointDist;
                                minVec = contours[i][j];
                            }
                        }
                    }
                    allPlaneDist.Add(minVec, planeMinDist);
                }
            }

            return allPlaneDist.Values.Min();
        }

        /// <summary>
        /// Calculates the length along an IMPT beam's CAX that traverses a certain structure.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        public static double[] GetBeamCAXPath(PlanSetup plan, Structure structure)
        {

            // initiate array. count number of actual treatment beams
            double[] beamPaths = new double[plan.Beams.Count(b => !b.IsSetupField)];

            // if not proton polan return nan
            if (!(plan.PlanType == PlanType.ExternalBeam_Proton))
            {
                return beamPaths;
            }

            // cast as ionplan
            var ionPlan = (IonPlanSetup)plan;

            int beamCount = 0;
            foreach (var beam in ionPlan.IonBeams)
            {
                var cp = beam.IonControlPoints.First();

                var sourcePos = beam.GetSourceLocation(cp.GantryAngle);
                var isoPos = beam.IsocenterPosition;

                // initialize segment array with ca 1mm resolution
                System.Collections.BitArray segmentStride = new System.Collections.BitArray((int)Math.Floor(Math.Abs(GetDistance3D(isoPos, sourcePos))));
                var segment = structure.GetSegmentProfile(sourcePos, isoPos, segmentStride);

                // if structure is not traversed
                if (!segment.Any(p => p.Value == true))
                {
                    return beamPaths;
                }

                // find points where beam CAX enters and exits structure
                var entryPoint = segment.First(p => p.Value == true);
                var exitPoint = segment.Last(p => p.Value == true);

                beamPaths[beamCount] = GetDistance3D(entryPoint.Position, exitPoint.Position);

                beamCount++;
            }

            return beamPaths;
        }
        #endregion

        // ideas for additions:

        // subvolume DVH
        // dose-distance penalty
    }
}
