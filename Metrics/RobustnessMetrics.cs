﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using OxyPlot;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace Metrics
{
    // Methods to quantify plan robustness using Eclipse's uncertainty scenarios
    public static class RobustnessMetrics
    {
        /// <summary>
        /// Returns the cumulative voxelwise minimum DVH as proposed by UMCG. DVH is returned as a list of OxyPlot DataPoint objects.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        public static List<DataPoint> CreateVoxelwiseMinDVH(PlanSetup plan, Structure structure)
        {
            // Calculate cumulative DVH
            var VoxelwiseMinDose = GetVoxelwiseMinDoseOneD(plan, structure);
            var OrderedMinDose = VoxelwiseMinDose.OrderByDescending(dose => dose);
            var CumSum = new List<double>();

            var points = new List<DataPoint>();

            for (double dose = 0; dose < VoxelwiseMinDose.Max(); dose += 0.1)
            {
                int NoOfVoxels = VoxelwiseMinDose.Count(vox => vox >= dose);
                CumSum.Add(NoOfVoxels);

                double VolumePercentage = (double)NoOfVoxels / (double)VoxelwiseMinDose.Count() * 100;
                DataPoint point = new DataPoint(dose, VolumePercentage);
                points.Add(point);
            }

            return points;
        }

        /// <summary>
        /// Get the minimum dose over all uncertainty scenarios for each voxel and return a 1D array.
        /// </summary>
        /// <param name="structure">Structure</param>
        /// <param name="plan">PlanSetup</param>
        /// <returns>1D double[]</returns>
        public static double[] GetVoxelwiseMinDoseOneD(PlanSetup plan, Structure structure)
        {
            // if plan does not have plan uncertainties calculated
            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null))
            {
                return null;
            }

            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var dose = plan.Dose;

            // If no uncertainty scenarios are defined
            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null))
            {
                MessageBox.Show("The chosen plan has no uncertainty scenarios!");
                return new double[] { double.NaN };
            }

            //// Dose calculation grid size?
            //double xres = 1;
            //double yres = 1;
            //double zres = 1;

            //// number of voxels
            //int xcount = (int)((dose.XRes * dose.XSize) / xres);
            //int ycount = (int)((dose.YRes * dose.YSize) / yres);
            //int zcount = (int)((dose.ZRes * dose.ZSize) / zres);
            int xcount = dose.XSize;
            int ycount = dose.YSize;
            int zcount = dose.ZSize;
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseArray = new double[xcount]; // dummy array for profile method

            DoseValue.DoseUnit doseUnit = dose.DoseMax3D.Unit;
            var doseOneD = new List<double>(); // 1D array with length = no of voxels

            for (int z = 0; z < dose.ZSize; z += 1)
            {
                for (int y = 0; y < dose.YSize; y += 1)
                {
                    VVector start = dose.Origin +
                                    dose.YDirection * y * dose.YRes +
                                    dose.ZDirection * z * dose.ZRes;
                    VVector end = start + dose.XDirection * dose.XRes * dose.XSize;



                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;
                    DoseProfile tempDoseProfile = null;

                    var minProfile = new double[segmentProfile.Count];

                    List<DoseProfile> uProfiles = new List<DoseProfile>();

                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = dose.GetDoseProfile(start, end, doseArray);
                                // start with nominal dose
                                for (int j = 0; j < doseProfile.Count; j++)
                                {
                                    minProfile[j] = doseProfile[j].Value;
                                }
                            }
                            if (uProfiles.Count == 0)
                            {
                                // Loop over uncertainty doses
                                foreach (var upln in plan.PlanUncertainties)
                                {
                                    if (upln.Dose != null) // skip empty udose copies
                                    {
                                        double[] udoseArray = new double[xcount]; // dummy array for profile method
                                        tempDoseProfile = upln.Dose.GetDoseProfile(start, end, udoseArray);
                                        uProfiles.Add(tempDoseProfile);
                                        tempDoseProfile = null;
                                        udoseArray = null;
                                    }
                                }
                            }


                            foreach (var uprof in uProfiles)
                            {
                                // if voxel doses are less than the current minimum, overwrite the current minimum
                                if (uprof[i].Value < minProfile[i])
                                    minProfile[i] = uprof[i].Value;
                            }

                            if (!Double.IsNaN(minProfile[i]))
                            {
                                doseOneD.Add(minProfile[i]);
                            }
                        }
                    }
                    doseProfile = null;
                    minProfile = null;
                    uProfiles.RemoveAll(uprof => uprof != null); // empty the list
                }
            }
            return doseOneD.ToArray();
        }

        /// <summary>
        /// Returns the voxelwise maximum DVH analogous to the voxelwise min DVH proposed by UMCG
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        public static List<DataPoint> CreateVoxelwiseMaxDVH(PlanSetup plan, Structure structure)
        {
            // if plan does not have plan uncertainties calculated
            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null))
            {
                return null;
            }

            // Calculate cumulative DVH
            var VoxelwiseMinDose = GetVoxelwiseMaxDoseOneD(plan, structure);
            var OrderedMinDose = VoxelwiseMinDose.OrderByDescending(dose => dose);
            var CumSum = new List<double>();

            var points = new List<DataPoint>();

            for (double dose = 0; dose < VoxelwiseMinDose.Max(); dose += 0.1)
            {
                int NoOfVoxels = VoxelwiseMinDose.Count(vox => vox >= dose);
                CumSum.Add(NoOfVoxels);

                double VolumePercentage = (double)NoOfVoxels / (double)VoxelwiseMinDose.Count() * 100;
                DataPoint point = new DataPoint(dose, VolumePercentage);
                points.Add(point);
            }

            return points;
        }

        /// <summary>
        /// Get the maximum dose over all uncertainty scenarios for each voxel and return a 1D array.
        /// </summary>
        /// <param name="structure">Structure</param>
        /// <param name="plan">PlanSetup</param>
        /// <returns>1D double[]</returns>
        public static double[] GetVoxelwiseMaxDoseOneD(PlanSetup plan, Structure structure)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var dose = plan.Dose;

            // If no uncertainty scenarios are defined
            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null))
            {
                MessageBox.Show("The chosen plan has no uncertainty scenarios!");
                return new double[] { double.NaN };
            }

            //// Dose calculation grid size?
            //double xres = 1;
            //double yres = 1;
            //double zres = 1;

            //// number of voxels
            //int xcount = (int)((dose.XRes * dose.XSize) / xres);
            //int ycount = (int)((dose.YRes * dose.YSize) / yres);
            //int zcount = (int)((dose.ZRes * dose.ZSize) / zres);
            int xcount = dose.XSize;
            int ycount = dose.YSize;
            int zcount = dose.ZSize;
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseArray = new double[xcount]; // dummy array for profile method

            DoseValue.DoseUnit doseUnit = dose.DoseMax3D.Unit;
            var doseOneD = new List<double>(); // 1D array with length = no of voxels

            for (int z = 0; z < dose.ZSize; z += 1)
            {
                for (int y = 0; y < dose.YSize; y += 1)
                {
                    VVector start = dose.Origin +
                                    dose.YDirection * y * dose.YRes +
                                    dose.ZDirection * z * dose.ZRes;
                    VVector end = start + dose.XDirection * dose.XRes * dose.XSize;



                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;
                    DoseProfile tempDoseProfile = null;

                    var minProfile = new double[segmentProfile.Count];

                    List<DoseProfile> uProfiles = new List<DoseProfile>();

                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = dose.GetDoseProfile(start, end, doseArray);
                                // start with nominal dose
                                for (int j = 0; j < doseProfile.Count; j++)
                                {
                                    minProfile[j] = doseProfile[j].Value;
                                }
                            }
                            // get uprofiles
                            if (uProfiles.Count == 0)
                            {
                                // Loop over uncertainty doses
                                foreach (var upln in plan.PlanUncertainties)
                                {
                                    if (upln.Dose != null) // skip empty udose copies
                                    {
                                        double[] udoseArray = new double[xcount]; // dummy array for profile method
                                        tempDoseProfile = upln.Dose.GetDoseProfile(start, end, udoseArray);
                                        uProfiles.Add(tempDoseProfile);
                                        tempDoseProfile = null;
                                        udoseArray = null;
                                    }
                                }
                            }


                            foreach (var uprof in uProfiles)
                            {
                                // if voxel doses are more than the current maximum, overwrite the current maximum
                                if (uprof[i].Value > minProfile[i])
                                    minProfile[i] = uprof[i].Value;
                            }

                            if (!Double.IsNaN(minProfile[i]))
                            {
                                doseOneD.Add(minProfile[i]);
                            }
                        }
                    }
                    doseProfile = null;
                    minProfile = null;
                    uProfiles.RemoveAll(uprof => uprof != null); // empty the list
                }
            }
            return doseOneD.ToArray();
        }

        /// <summary>
        /// Returns a 1D double array containing the difference (in Gy) between the minimum and maximum dose values for each voxel
        /// </summary>
        /// <param name="structure">Structure</param>
        /// <param name="plan">PlanSetup</param>
        /// <returns>double[]</returns>
        public static double[] GetVoxelwiseErrorBarWidth(Structure structure, PlanSetup plan)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var dose = plan.Dose;

            // If no uncertainty scenarios are defined
            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null))
            {
                MessageBox.Show("The chosen plan has no uncertainty scenarios!");
                return new double[] { double.NaN };
            }


            int xcount = dose.XSize;
            int ycount = dose.YSize;
            int zcount = dose.ZSize;
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseArray = new double[xcount]; // dummy array for profile method
            DoseValue.DoseUnit doseUnit = dose.DoseMax3D.Unit;
            var doseOneD = new List<double>(); // 1D array with length = no of voxels



            for (int z = 0; z < dose.ZSize; z += 1)
            {
                for (int y = 0; y < dose.YSize; y += 1)
                {
                    VVector start = dose.Origin +
                                    dose.YDirection * y * dose.YRes +
                                    dose.ZDirection * z * dose.ZRes;
                    VVector end = start + dose.XDirection * dose.XRes * dose.XSize;



                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;
                    DoseProfile tempDoseProfile = null;
                    var minProfile = new double[segmentProfile.Count];
                    var maxProfile = new double[segmentProfile.Count];

                    List<DoseProfile> uProfiles = new List<DoseProfile>();

                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = dose.GetDoseProfile(start, end, doseArray);
                            }
                            if (uProfiles.Count == 0)
                            {
                                // Loop over uncertainty doses
                                foreach (var upln in plan.PlanUncertainties)
                                {
                                    if (upln.Dose != null) // skip empty udose copies
                                    {
                                        double[] udoseArray = new double[xcount]; // dummy array for profile method
                                        tempDoseProfile = upln.Dose.GetDoseProfile(start, end, udoseArray);
                                        uProfiles.Add(tempDoseProfile);
                                        tempDoseProfile = null;
                                        udoseArray = null;
                                    }
                                }
                            }

                            // start with nominal dose
                            for (int j = 0; j < doseProfile.Count; j++)
                            {
                                minProfile[j] = doseProfile[j].Value;
                                maxProfile[j] = doseProfile[j].Value;
                            }
                            foreach (var uprof in uProfiles)
                            {
                                // if voxel doses are less than the current minimum, overwrite the current minimum
                                if (uprof[i].Value < minProfile[i])
                                    minProfile[i] = uprof[i].Value;
                                else if (uprof[i].Value > maxProfile[i])
                                    maxProfile[i] = uprof[i].Value;
                            }

                            // save difference between max and min value for each voxel
                            if (!Double.IsNaN(minProfile[i]) & !Double.IsNaN(maxProfile[i]))
                            {
                                doseOneD.Add(maxProfile[i] - minProfile[i]);
                            }
                        }
                    }
                    doseProfile = null;
                    minProfile = null;
                    maxProfile = null;
                    uProfiles.RemoveAll(uprof => uprof != null); // empty the list
                }
            }
            return doseOneD.ToArray();
        }

        /// <summary>
        /// Gets cumulative dose error bar histogram for a plan and structure
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        public static List<DataPoint> GetVoxelwiseCumulativeEBHistogram(PlanSetup plan, Structure structure)
        {
            // If no uncertainty scenarios are defined
            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null))
            {
                MessageBox.Show("The chosen plan has no uncertainty scenarios!");
                return null;
            }

            var ebList = GetVoxelwiseErrorBarWidth(structure, plan).ToList();
            double numberOfVoxels = (double)ebList.Count();
            ebList.Sort(); // sort values from lowest to highest
            ebList.Reverse(); // sorts the list from highest to lowest

            var ebHist = new List<DataPoint>();
            for (double d = 0; d <= ebList.Max(); d += 0.1)
            {
                ebHist.Add(new DataPoint(d, ebList.Count(val => val >= d) / numberOfVoxels * 100.0 * plan.Dose.XRes * plan.Dose.YRes * plan.Dose.ZRes / 1000.0));
            }

            return ebHist;
        }

        /// <summary>
        /// Returns a 1D array containing the maximum dose deviation (in Gy) from the nominal dose for each voxel
        /// </summary>
        /// <param name="structure">Structure</param>
        /// <param name="plan">PlanSetup</param>
        /// <returns>double[]</returns>
        public static double[] GetVoxelwiseMaximumDeviation(Structure structure, PlanSetup plan)
        {
            var ErrorBarWidth = new double[plan.Dose.XSize * plan.Dose.YSize * plan.Dose.ZSize];

            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var dose = plan.Dose;

            // If no uncertainty scenarios are defined
            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null))
            {
                MessageBox.Show("The chosen plan has no uncertainty scenarios!");
                return new double[] { double.NaN };
            }


            // number of voxels
            int xcount = dose.XSize;
            int ycount = dose.YSize;
            int zcount = dose.ZSize;
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseArray = new double[xcount]; // dummy array for profile method
            DoseValue.DoseUnit doseUnit = dose.DoseMax3D.Unit;
            var MaxDevOneD = new List<double>(); // 1D array with length = no of voxels



            for (int z = 0; z < dose.ZSize; z += 1)
            {
                for (int y = 0; y < dose.YSize; y += 1)
                {
                    VVector start = dose.Origin +
                                    dose.YDirection * y * dose.YRes +
                                    dose.ZDirection * z * dose.ZRes;
                    VVector end = start + dose.XDirection * dose.XRes * dose.XSize;



                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;
                    DoseProfile tempDoseProfile = null;
                    var maxDevProfile = new double[segmentProfile.Count];

                    List<DoseProfile> uProfiles = new List<DoseProfile>();

                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = dose.GetDoseProfile(start, end, doseArray);
                            }
                            if (uProfiles.Count == 0)
                            {
                                // Loop over uncertainty doses
                                foreach (var upln in plan.PlanUncertainties)
                                {
                                    if (upln.Dose != null) // skip empty udose copies
                                    {
                                        double[] udoseArray = new double[xcount]; // dummy array for profile method
                                        tempDoseProfile = upln.Dose.GetDoseProfile(start, end, udoseArray);
                                        uProfiles.Add(tempDoseProfile);
                                        tempDoseProfile = null;
                                        udoseArray = null;
                                    }
                                }
                            }

                            // start with nominal dose
                            for (int j = 0; j < doseProfile.Count; j++)
                            {
                                maxDevProfile[j] = 0;
                            }

                            foreach (var uprof in uProfiles)
                            {
                                if (Math.Abs(uprof[i].Value - doseProfile[i].Value) > maxDevProfile[i])
                                {
                                    maxDevProfile[i] = uprof[i].Value - doseProfile[i].Value;
                                }
                            }

                            if (!Double.IsNaN(maxDevProfile[i]))
                            {
                                MaxDevOneD.Add(maxDevProfile[i]);
                            }
                        }
                    }
                    doseProfile = null;
                    maxDevProfile = null;
                    uProfiles.RemoveAll(uprof => uprof != null); // empty the list
                }
            }
            return MaxDevOneD.ToArray();
        }

        /// <summary>
        /// Method that returns the worst case value for a certain D(V)
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="volume"></param>
        /// <param name="volumePresentation"></param>
        /// <returns></returns>
        public static Tuple<double, string> GetWorstCaseDoseAtVolume(PlanSetup plan, Structure structure, double volume, VolumePresentation volumePresentation)
        {
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, volumePresentation, 0.1);

            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null) | plan.PlanType != PlanType.ExternalBeam_Proton)
            {
                MessageBox.Show("Plan has no planuncertainties calculated!");
                return new Tuple<double, string>(DVHMetrics.DoseFromDVH(dvh, 98), "Nominal");
            }


            DVHData udvh;
            double worstCaseVal = DVHMetrics.DoseFromDVH(dvh, volume);
            double tempVal = worstCaseVal;
            string worstCaseScenario = "Nominal";

            foreach (var upln in plan.PlanUncertainties)
            {
                if (upln.Dose != null)
                {
                    udvh = upln.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, volumePresentation, 0.1);
                    tempVal = DVHMetrics.DoseFromDVH(udvh, volume);

                    if (tempVal < worstCaseVal)
                    {
                        worstCaseVal = tempVal;
                        worstCaseScenario = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    }
                }
            }

            return new Tuple<double, string>(worstCaseVal, worstCaseScenario);
        }

        /// <summary>
        /// Method that returns the worst case maximum dose from a DVH
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        public static Tuple<double, string> GetWorstCaseMaxDose(PlanSetup plan, Structure structure)
        {
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null) | plan.PlanType != PlanType.ExternalBeam_Proton)
            {
                MessageBox.Show("Plan has no planuncertainties calculated!");
                return new Tuple<double, string>(dvh.MaxDose.Dose, "Nominal");
            }


            DVHData udvh;
            double worstCaseVal = dvh.MaxDose.Dose;
            double tempVal = worstCaseVal;
            string worstCaseScenario = "Nominal";

            foreach (var upln in plan.PlanUncertainties)
            {
                if (upln.Dose != null)
                {
                    udvh = upln.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    tempVal = udvh.MaxDose.Dose;

                    if (tempVal > worstCaseVal)
                    {
                        worstCaseVal = tempVal;
                        worstCaseScenario = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    }
                }
            }

            return new Tuple<double, string>(worstCaseVal, worstCaseScenario);
        }

        // Ideas for additions:

        // DVH area

        // Relative volume histogram

        // No of scenarios in which main constraint is not met
        // define helper method in which DAHANCA / DNOG constraints are hard coded. Message box to show to user which cstr is evaluated
    }
}
